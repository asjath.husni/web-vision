import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';

import { ResizableModule } from 'angular-resizable-element';

import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { getFunctions, provideFunctions } from '@angular/fire/functions';
import { getStorage, provideStorage } from '@angular/fire/storage';
import { environment } from 'src/environments/environment';
import { FileUploadComponent } from './components/file-upload/file-upload.component';

import { FilePondModule, registerPlugin } from 'ngx-filepond';
import * as FilePondPluginImagePreview from 'filepond-plugin-image-preview';
import * as FilePondPluginFileValidateType from 'filepond-plugin-file-validate-type';
import { ImgViewerComponent } from './components/img-viewer/img-viewer.component';
import { BbTagsComponent } from './components/img-viewer/bb-tags/bb-tags.component';
import { FirebaseFunctionsServices } from './services/firebase-function.services';
import { ImgEditorComponent } from './components/img-editor/img-editor.component';
import { FirestoreServices } from './services/firebase-firestore.services';
import { StorageServices } from './services/firebase-storage.services';

registerPlugin(FilePondPluginImagePreview, FilePondPluginFileValidateType);

@NgModule({
  declarations: [
    AppComponent,
    FileUploadComponent,
    ImgViewerComponent,
    BbTagsComponent,
    ImgEditorComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,

    FilePondModule,

    // Material UI Modules
    MatToolbarModule,
    MatIconModule,
    MatGridListModule,
    MatButtonModule,
    MatDividerModule,
    DragDropModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatCardModule,

    ResizableModule,

    // Firebase Modules
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    provideFunctions(() => getFunctions()),
    provideStorage(() => getStorage()),
  ],
  providers: [FirebaseFunctionsServices, FirestoreServices, StorageServices],
  bootstrap: [AppComponent],
})
export class AppModule {}
