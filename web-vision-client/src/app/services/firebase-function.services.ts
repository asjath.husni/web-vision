import { Injectable, isDevMode } from '@angular/core';

import {
  httpsCallable,
  Functions,
  connectFunctionsEmulator,
} from '@angular/fire/functions';

@Injectable()
export class FirebaseFunctionsServices {
  constructor(private cloudfunctions: Functions) {
    if (isDevMode()) {
      connectFunctionsEmulator(this.cloudfunctions, '127.0.0.1', 5001);
    }
  }

  analyzeImages(file:any) {
    const analyzeImage = httpsCallable(this.cloudfunctions, 'analyzeImage');
    return analyzeImage({ file: file })
  }
}
