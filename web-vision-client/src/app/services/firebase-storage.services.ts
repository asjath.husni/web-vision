import { Injectable, isDevMode } from '@angular/core';
import {
  Storage,
  connectStorageEmulator,
  getDownloadURL,
  ref,
  uploadBytes,
} from '@angular/fire/storage';
interface Metadata {
  contentType: string;
  customMetadata?: {
    jobId?: string;
  };
}

@Injectable()
export class StorageServices {
  constructor(private storage: Storage) {
    if (isDevMode()) {
      connectStorageEmulator(storage, '127.0.0.1', 9199);
    }
  }

  async uploadVisionImage(file: File, jobId?: string) {
    const imageRef = ref(this.storage, file.name);
    let metadata: Metadata = {
      contentType: 'image/jpeg',
    };

    if (jobId) metadata.customMetadata = {jobId};

    try {
      const uploadedTask = await uploadBytes(imageRef, file, metadata);
      const downloadUrl = await getDownloadURL(uploadedTask.ref);
      return Promise.resolve(downloadUrl);
    } catch (error) {
      console.error(error);
      return Promise.reject(error);
    }
  }
}
