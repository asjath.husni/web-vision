import { Injectable, isDevMode } from '@angular/core';
import { v4 as uuidv4 } from 'uuid';

import {
  getDoc,
  connectFirestoreEmulator,
  Firestore,
  collection,
  collectionData,
  doc,
  onSnapshot,
  setDoc,
  updateDoc,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { BoundingBoxMetadata } from '../core/models/report.model';

@Injectable()
export class FirestoreServices {
  constructor(private fireStore: Firestore) {
    if (isDevMode()) {
      connectFirestoreEmulator(this.fireStore, '127.0.0.1', 8080);
    }
  }

  getAllVisionCollection() {
    const visionCollection = collection(this.fireStore, 'vision');
    const items = collectionData(visionCollection);
    return items;
  }

  getVisionById(id: string) {
    const docRef = doc(this.fireStore, 'vision', id);

    return new Observable((observer) => {
      return onSnapshot(
        docRef,
        (snapshot) => observer.next(snapshot.data()),
        (error) => observer.error(error.message)
      );
    });
  }

  async updateVisionById(id: string, metadata: BoundingBoxMetadata[]) {
    const docRef = doc(this.fireStore, 'vision', id);
    const result = await updateDoc(docRef, { outline: { metadata: metadata } });
    return result;
  }

  async addVision(payload: any, id?: string) {
    if (!id) id = uuidv4();
    return await setDoc(doc(this.fireStore, 'vision', id), payload);
  }
}
