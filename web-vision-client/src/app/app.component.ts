import {
  Component,
  inject,
  OnInit,
  isDevMode,
  ViewChild,
  ElementRef,
} from '@angular/core';

import {
  httpsCallable,
  Functions,
  connectFunctionsEmulator,
} from '@angular/fire/functions';
import { FirebaseFunctionsServices } from './services/firebase-function.services';
import { FirestoreServices } from './services/firebase-firestore.services';
import { StorageServices } from './services/firebase-storage.services';

import { v4 as uuidv4 } from 'uuid';
import { Report } from './core/models/report.model';
import { take, timeout } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'web-vision';

  @ViewChild('editor') editor!: ElementRef<HTMLElement>;

  showImageEditor = false;
  addedImageFile: File | null = null;
  imageSource =
    'https://globaleducationplatform.com/wp-content/uploads/2022/04/placeholder-2.png';
  isUploadOnProgress = false;

  currentReport: Report | null = null;

  allReports: Report[] | null = null;

  constructor(
    private firestoreService: FirestoreServices,
    private firebaseStorageService: StorageServices,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.firestoreService.getAllVisionCollection().subscribe((items) => {
      this.allReports = items as Report[];
    });
  }

  startAnalyzing() {
    if (!this.addedImageFile) {
      this._snackBar.open('Please upload a File', 'Close', {
        duration: 3000,
      });
      return;
    }

    this.showImageEditor = true;
    this.isUploadOnProgress = true;

    const jobId = uuidv4();

    this.firebaseStorageService
      .uploadVisionImage(this.addedImageFile, jobId)
      .then((imageUrl) => {
        this.imageSource = imageUrl;
        //add handler for document change
        this.firestoreService
          .getVisionById(jobId)
          .pipe(take(2), timeout({ each: 30 * 1000 }))
          .subscribe({
            next: (data) => {
              if (data) {
                const analysisData = data as Report;
                this.currentReport = analysisData;
                this.isUploadOnProgress = false;
              }
            },
            error: (err) => {
              console.error(err);
              if (err.name == 'TimeoutError') {
                this.isUploadOnProgress = false;
                this.showImageEditor = false;

                this._snackBar.open('Network Request Timed Out', 'Close', {
                  duration: 3000,
                });
              }
            },
          });
      });
  }

  selectItem(index: number) {
    if (this.allReports) {
      this.currentReport = this.allReports[index];
      this.showImageEditor = true;

      if (this.editor) {
        this.editor.nativeElement.scrollIntoView({behavior:'smooth'});
      }
    }
  }
}
