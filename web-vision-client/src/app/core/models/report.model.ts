export interface BoundingBox{
    xmin: number;
    ymin: number;
    xmax: number;
    ymax: number;
}


export interface BoundingBoxMetadata {
  confidence: number;
  class: string;
  bbox: BoundingBox;
}

export interface Report {
  incident_id : string,
  scene: {
    file_name: string;
    file_type: string;
    file_source : string;
  };
  outline: {
    job_id: string;
    job_result: string;
    url: string;
    created_at: string;
    deadline_at: string;
    completed_at: string;
    vehicle_movement: boolean;
    person_assessment: string;
    metadata: BoundingBoxMetadata[];
  };
}
