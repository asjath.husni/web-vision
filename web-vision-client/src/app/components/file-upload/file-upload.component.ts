import {
  Component,
  inject,
  OnInit,
  ViewChild,
  isDevMode,
  Output,
  EventEmitter,
} from '@angular/core';
import { FilePondOptions } from 'filepond';
import { FilePondComponent } from 'ngx-filepond';
import {
  httpsCallable,
  Functions,
  connectFunctionsEmulator,
} from '@angular/fire/functions';
import { FirebaseFunctionsServices } from 'src/app/services/firebase-function.services';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit {
  @ViewChild('myPond')
  myPond!: FilePondComponent;

  constructor(private cFunctions: FirebaseFunctionsServices) {}

  @Output()
  onFileAdded = new EventEmitter<any>();

  ngOnInit(): void {}

  pondOptions: FilePondOptions = {
    allowMultiple: false,
    labelIdle: 'Add or Drop your file here...',
    credits: false,
    allowImagePreview : false,
    acceptedFileTypes : ['image/*']
  };

  pondFiles: FilePondOptions['files'] = [];

  pondHandleInit() {

  }

  pondHandleAddFile(event: any) {
    this.onFileAdded.emit(event.file.file);
  }

  pondHandleActivateFile(event: any) {
  }

  pondHandleRemoveFile(event: any){
    this.onFileAdded.emit(null);
  }

}
