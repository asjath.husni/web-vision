import { Component, ElementRef, ViewChild, AfterViewInit, OnInit, Input, HostListener, Output, EventEmitter } from '@angular/core';
import { BoundingBoxMetadata } from 'src/app/core/models/report.model';

@Component({
  selector: 'app-img-viewer',
  templateUrl: './img-viewer.component.html',
  styleUrls: ['./img-viewer.component.scss'],
})
export class ImgViewerComponent implements AfterViewInit {

  @ViewChild('image_wrapper') imageRef!: ElementRef<HTMLElement>;

  @Input()
  dataSource = ''

  topIndex = -1;

  @Input()
  bboxList : BoundingBoxMetadata[] = [];

  @Output()
  bboxListChange = new EventEmitter<BoundingBoxMetadata[]>();

  ngAfterViewInit(): void {
    console.table(this.bboxList);

  }

  removeBoundingBox(index: number) {
    this.bboxList.splice(index, 1);
  }

  setTopIndex(index: number) {
    this.topIndex = index;
  }
}
