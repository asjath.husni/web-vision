import { CdkDragEnd } from '@angular/cdk/drag-drop';
import {
  Component,
  Input,
  Output,
  OnInit,
  AfterViewInit,
  EventEmitter,
  ViewChild,
  ElementRef,
  Renderer2,
} from '@angular/core';

import { ResizeEvent } from 'angular-resizable-element';
import { BoundingBox } from 'src/app/core/models/report.model';

@Component({
  selector: 'app-bb-tags',
  templateUrl: './bb-tags.component.html',
  styleUrls: ['./bb-tags.component.scss'],
})
export class BbTagsComponent implements OnInit, AfterViewInit {
  @ViewChild('bbTag') tagElement: ElementRef<HTMLElement> | undefined;

  @Input()
  bbox: BoundingBox = {
    xmin: 0,
    ymin: 0,
    xmax: 0,
    ymax: 0,
  };

  @Input('index')
  index = 0;

  @Input('onTop')
  isOnTop = false;

  @Output()
  closeBoundingBox = new EventEmitter<number>();

  @Output()
  changeTopIndex = new EventEmitter<number>();

  @Output()
  bboxChange = new EventEmitter<BoundingBox>();

  initialPositionStyle: Record<string, string | undefined | null> | null = null;

  constructor(private renderer:Renderer2){}

  ngOnInit(): void {
    const bbox = {
      top: 100 * this.bbox.ymin + '%',
      left: 100 * this.bbox.xmin + '%',
      width: 100 * (this.bbox.xmax - this.bbox.xmin) + '%',
      height: 100 * (this.bbox.ymax - this.bbox.ymin) + '%',
    };

    this.initialPositionStyle = bbox;
  }

  ngAfterViewInit(): void {
  }

  closeBoundingBoxHandle(index: number) {
    this.closeBoundingBox.emit(index);
  }

  getElemetTop(index: number) {
    this.changeTopIndex.emit(index);
  }

  dragEnded(event: CdkDragEnd) {
    if (this.tagElement) {
      let relativePos = {
        xmin: 0,
        ymin: 0,
        xmax: 0,
        ymax: 0,
      };

      const sourceElemntBoundingBox =
        event.source.element.nativeElement.getBoundingClientRect();

      const parentElemntBoundingBox =
        event.source.element.nativeElement.offsetParent?.getBoundingClientRect() ||
        sourceElemntBoundingBox;

      relativePos = {
        xmin:
          Math.round(
            ((sourceElemntBoundingBox.x - parentElemntBoundingBox.x) /
              parentElemntBoundingBox.width) *
              1000
          ) / 1000,

        xmax:
          Math.round(
            ((sourceElemntBoundingBox.x +
              sourceElemntBoundingBox.width -
              parentElemntBoundingBox.x) /
              parentElemntBoundingBox.width) *
              1000
          ) / 1000,

        ymin:
          Math.round(
            ((sourceElemntBoundingBox.y - parentElemntBoundingBox.y) /
              parentElemntBoundingBox.height) *
              1000
          ) / 1000,

        ymax:
          Math.round(
            ((sourceElemntBoundingBox.y +
              sourceElemntBoundingBox.height -
              parentElemntBoundingBox.y) /
              parentElemntBoundingBox.height) *
              1000
          ) / 1000,
      };

      const absRelativePos = {
        xmin: Math.abs(relativePos.xmin),
        xmax: Math.abs(relativePos.xmax),
        ymin: Math.abs(relativePos.ymin),
        ymax: Math.abs(relativePos.ymax),
      };

      console.table(absRelativePos);

      this.bbox = absRelativePos;
    }
    this.saveChanges();
  }

  onResizeEnd(event: ResizeEvent): void {
    let newWidth = event.rectangle.width || 0;
    let newHeight = event.rectangle.height || 0;

    if (this.tagElement) {
      let relativePos = {
        xmin: 0,
        ymin: 0,
        xmax: 0,
        ymax: 0,
      };

      const sourceElemntBoundingBox =
        this.tagElement.nativeElement.getBoundingClientRect();

      const parentElemntBoundingBox =
        this.tagElement.nativeElement.offsetParent?.getBoundingClientRect() ||
        sourceElemntBoundingBox;

      relativePos = {
        xmin:
          (sourceElemntBoundingBox.x - parentElemntBoundingBox.x) /
          parentElemntBoundingBox.width,
        xmax:
          (sourceElemntBoundingBox.x + newWidth - parentElemntBoundingBox.x) /
          parentElemntBoundingBox.width,
        ymin:
          (sourceElemntBoundingBox.y - parentElemntBoundingBox.y) /
          parentElemntBoundingBox.height,
        ymax:
          (sourceElemntBoundingBox.y + newHeight - parentElemntBoundingBox.y) /
          parentElemntBoundingBox.height,
      };

      const absRealivePos = {
        xmin: Math.abs(Math.round(relativePos.xmin * 1000) / 1000),
        xmax:
          relativePos.xmax > 1
            ? 1
            : Math.abs(Math.round(relativePos.xmax * 1000) / 1000),
        ymin: Math.abs(Math.round(relativePos.ymin * 1000) / 1000),
        ymax:
          relativePos.ymax > 1
            ? 1
            : Math.abs(Math.round(relativePos.ymax * 1000) / 1000),
      };

      this.bbox = absRealivePos;
      console.table(absRealivePos);

      this.renderElement(this.bbox);
    }
    this.saveChanges();
  }

  renderElement(bobox: BoundingBox) {
    this.renderer.setStyle(this.tagElement?.nativeElement, 'width', 100 * (bobox.xmax - bobox.xmin) + '%')
    this.renderer.setStyle(this.tagElement?.nativeElement, 'height', 100 * (bobox.ymax - bobox.ymin) + '%')
  }

  saveChanges() {
    const snapshotBoundingbox = this.bbox;
    this.bboxChange.emit(snapshotBoundingbox);
  }
}
