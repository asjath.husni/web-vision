import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BbTagsComponent } from './bb-tags.component';

describe('BbTagsComponent', () => {
  let component: BbTagsComponent;
  let fixture: ComponentFixture<BbTagsComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BbTagsComponent]
    });
    fixture = TestBed.createComponent(BbTagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
