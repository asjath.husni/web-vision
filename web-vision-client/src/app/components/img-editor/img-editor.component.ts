import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { BoundingBoxMetadata, Report } from 'src/app/core/models/report.model';
import * as _ from 'lodash';
import { FirestoreServices } from 'src/app/services/firebase-firestore.services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-img-editor',
  templateUrl: './img-editor.component.html',
  styleUrls: ['./img-editor.component.scss'],
})
export class ImgEditorComponent implements OnInit, OnChanges {
  @Input()
  isHidden = false;

  @Input()
  srcImage = '';

  @Input()
  isLoading = false;

  @Input()
  report: Report | null = null;

  bboxlistSnapshot: BoundingBoxMetadata[] = [];

  bboxlist: BoundingBoxMetadata[] =
    this.report != null ? this.report.outline.metadata : [];

  constructor(
    private firestoreServices: FirestoreServices,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    if(this.report){
      this.bboxlist = this.report.outline.metadata ;
      this.srcImage = this.report.scene.file_source;
      this.bboxlistSnapshot = _.cloneDeep(this.bboxlist);
    }

  }

  async saveChanges() {

    if (this.report) {
      await this.firestoreServices.updateVisionById(
        this.report.incident_id,
        this.bboxlist
      );
      this._snackBar.open('Changes Saved Successfully', 'Close', {
        duration: 3000,
      });
      this.bboxlistSnapshot = _.cloneDeep(this.bboxlist);
    } else {
      console.error('Unable to save changes');
    }
  }

  cancelChanges() {
    this.bboxlist = _.cloneDeep(this.bboxlistSnapshot);
  }
}
