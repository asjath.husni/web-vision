import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImgEditorComponent } from './img-editor.component';

describe('ImgEditorComponent', () => {
  let component: ImgEditorComponent;
  let fixture: ComponentFixture<ImgEditorComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ImgEditorComponent]
    });
    fixture = TestBed.createComponent(ImgEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
