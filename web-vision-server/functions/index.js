/* eslint-disable require-jsdoc */
const {logger} = require("firebase-functions");
const {onObjectFinalized} = require("firebase-functions/v2/storage");
const {setGlobalOptions} = require("firebase-functions/v2");

const {initializeApp} = require("firebase-admin/app");
const {getStorage} = require("firebase-admin/storage");
const {getFirestore} = require("firebase-admin/firestore");

const fetch = require("node-fetch");
const FormData = require("form-data");

require("dotenv").config();

setGlobalOptions({maxInstances: 4});
initializeApp();

class HTTPResponseError extends Error {
  constructor(response) {
    super(`HTTP Error Response: ${response.status} ${response.statusText}`);
    this.response = response;
  }
}

const checkStatus = (response) => {
  if (response.ok) {
    return response;
  } else {
    throw new HTTPResponseError(response);
  }
};

exports.recordIncident = onObjectFinalized({cpu: 2}, async (event) => {
  const fileBucket = event.data.bucket;
  const filePath = event.data.name;
  const contentType = event.data.contentType;

  if (!contentType.startsWith("image/")) {
    return logger.log("This is not an image.");
  }

  const bucket = getStorage().bucket(fileBucket);
  const fileResponse = await bucket.file(filePath);
  const downloadResponse = await fileResponse.download();
  const imageBuffer = downloadResponse[0];
  logger.log("Image downloaded!");

  const fileMetadata = await fileResponse.getMetadata();
  const metadata = fileMetadata[0];
  logger.log(metadata);

  await fileResponse.makePublic();
  const filePublicUrl = fileResponse.publicUrl();
  logger.log(filePublicUrl);

  const formdata = new FormData();
  formdata.append("image", imageBuffer, "example.jpeg");

  try {
    const promiseqResponse = await fetch(
        process.env.PROMISEQ_BASE_URL + "/sendToThreatDetect",
        {
          method: "POST",
          headers: {
            "client-id": process.env.PROMISEQ_CLIENT_ID,
            "promiseq-subscription-key": process.env.PROMISEQ_SUBSCRIPTION_KEY,
          },
          body: formdata,
        },
    );
    const response = checkStatus(promiseqResponse);
    const promiseqResponseData = await response.json();
    logger.log(promiseqResponseData);

    const document = await getFirestore()
        .doc("vision/" + metadata.metadata.jobId)
        .set({
          incident_id: metadata.metadata.jobId,
          scene: {
            file_name: metadata.name,
            file_type: metadata.contentType,
            file_source: filePublicUrl,
          },
          outline: promiseqResponseData,
        });
    console.log(document);
  } catch (error) {
    const errorBody = await error.response.text();
    logger.error(errorBody);
  }

  return logger.log("Analysis Complete");
});
